package com.example.pixelart.game;

public class ImageCell {
    private int color, currentColor;

    public ImageCell(int color) {
        this.color = color;
        this.currentColor = -1;
    }

    public boolean paintedRight() {
        return color == currentColor;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public int getCurrentColor() {
        return currentColor;
    }

    public void setCurrentColor(int currentColor) {
        this.currentColor = currentColor;
    }
}
