package com.example.pixelart.game;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;

import com.example.pixelart.R;

import java.util.ArrayList;

public class PaletteView extends View {
    private int color_x_size = 75;
    private int color_y_size = 50;

    private float mHeight;
    private float mWidth;
    private Drawable checkMark;
    private int chosenColor = 0;
    private float offset = 0;
    private float moveX, moveY;
    private boolean click = false;

    private ArrayList<Integer> palette;
    private ArrayList<Boolean> complete = new ArrayList<>();

    private ClickCallback callback;

    public PaletteView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        checkMark = ResourcesCompat.getDrawable(context.getResources(), R.drawable.ic_baseline_check_24,null);
    }

    @Override
    protected void onDraw(Canvas c) {
        if(mWidth > 0 && palette != null){
            drawPalette(c);
        }
    }

    @Override
    protected void onSizeChanged(int xNew, int yNew, int xOld, int yOld) {
        super.onSizeChanged(xNew, yNew, xOld, yOld);
        if(getWidth() > 0) initialise();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if(event.getActionMasked() == MotionEvent.ACTION_DOWN) {
            click = true;
            moveX = event.getX();
            moveY = event.getY();
            return true;
        } else if(event.getActionMasked() == MotionEvent.ACTION_MOVE) {
            click = false;
            move(event.getX() - moveX,event.getY() - moveY);
            moveX = event.getX();
            moveY = event.getY();
            return true;
        } else if(event.getActionMasked() == MotionEvent.ACTION_UP) {
            if(click) handleClick(event.getX(), event.getY());
            click = false;
            return true;
        }
        return super.onTouchEvent(event);
    }

    public void setPalette(ArrayList<Integer> palette) {
        this.palette = palette;
        for(int i = 0; i < palette.size(); i++) {
            this.complete.add(false);
        }
        measureSize();
    }

    public void setCallback(ClickCallback callback) {
        this.callback = callback;
    }

    public void updateCompletion(ArrayList<Boolean> complete) {
        this.complete = complete;
        invalidate();
    }

    private void initialise() {
        mWidth = getWidth();
        mHeight = getHeight();
        measureSize();
        offset = 0;
        invalidate();
    }

    public void setComplete(int n) {
        complete.set(n, true);
    }

    private void drawPalette(Canvas c) {
        Paint p = new Paint();
        p.setTextAlign(Paint.Align.CENTER);
        p.setTextSize((float)color_y_size / 2);
        if(mWidth > mHeight) {
            int rowCapacity = (int) ((palette.size() / (mHeight / color_y_size)) + (palette.size() % (mHeight / color_y_size)));
            for(int n = 0; n < palette.size(); n++) {
                p.setColor(palette.get(n));
                p.setStyle(Paint.Style.FILL);
                c.drawRect((n % rowCapacity) * color_x_size - offset, ((float)(n / rowCapacity) * color_y_size),
                        ((n % rowCapacity) + 1) * color_x_size - offset, ((float)(n / rowCapacity) * color_y_size) + color_y_size, p);

                if(n == chosenColor) {
                    p.setColor(Color.RED);
                    p.setStrokeWidth(5);
                }
                else {
                    p.setColor(Color.BLACK);
                    p.setStrokeWidth(3);
                }
                p.setStyle(Paint.Style.STROKE);
                c.drawRect((n % rowCapacity) * color_x_size - offset, ((float)(n / rowCapacity) * color_y_size),
                        ((n % rowCapacity) + 1) * color_x_size - offset, (((float)(n / rowCapacity) + 1) * color_y_size), p);
                if(complete.get(n)) {
                    checkMark.setBounds((int) ((n % rowCapacity) * color_x_size - offset + ((color_x_size - color_y_size) / 2)), ((n / rowCapacity) * color_y_size),
                            (int) (((n % rowCapacity) + 1) * color_x_size - offset - ((color_x_size - color_y_size) / 2)), ((n / rowCapacity) * color_y_size) + color_y_size);
                    checkMark.draw(c);
                } else {
                    p.setColor(Color.BLACK);
                    p.setStyle(Paint.Style.FILL);
                    c.drawText(String.valueOf(n + 1), ((n % rowCapacity) * color_x_size) + (float)(color_x_size / 2) - offset,
                            ((float)(n / rowCapacity) * color_y_size) + (float)(color_y_size / 3 * 2), p);
                }
            }
        } else {
            int columnCapacity = (int) ((palette.size() / (mWidth / color_x_size)) + (palette.size() % (mWidth / color_x_size)));
            for(int n = 0; n < palette.size(); n++) {
                p.setColor(palette.get(n));
                p.setStyle(Paint.Style.FILL);
                c.drawRect( ((float)(n / columnCapacity) * color_x_size),(n % columnCapacity) * color_y_size - offset,
                        ((float)(n / columnCapacity) * color_x_size) + color_x_size,((n % columnCapacity) + 1) * color_y_size - offset,  p);

                if(n == chosenColor) {
                    p.setColor(Color.RED);
                    p.setStrokeWidth(5);
                }
                else {
                    p.setColor(Color.BLACK);
                    p.setStrokeWidth(3);
                }
                p.setStyle(Paint.Style.STROKE);
                c.drawRect(((float)(n / columnCapacity) * color_x_size),(n % columnCapacity) * color_y_size - offset,
                        ((float)(n / columnCapacity) * color_x_size) + color_x_size,((n % columnCapacity) + 1) * color_y_size - offset,  p);
                if(complete.get(n)) {
                    checkMark.setBounds(((n / columnCapacity) * color_x_size),(int)((n % columnCapacity) * color_y_size - offset + ((color_y_size - color_x_size) / 2)),
                            ((n / columnCapacity) * color_x_size) + color_x_size,(int)(((n % columnCapacity) + 1) * color_y_size - offset - ((color_y_size - color_x_size) / 2)));
                    checkMark.draw(c);
                } else {
                    p.setColor(Color.BLACK);
                    p.setStyle(Paint.Style.FILL);
                    c.drawText(String.valueOf(n + 1), (float)((n / columnCapacity) * color_x_size) + (float)(color_x_size / 2),
                            (n % columnCapacity * color_y_size) + (float)(color_y_size / 3 * 2) - offset, p);
                }
            }
        }
    }

    private void handleClick(float x, float y) {
        int n;
        if(mWidth > mHeight) {
            int rowCapacity = (int) ((palette.size() / (mHeight / color_y_size)) + (palette.size() % (mHeight / color_y_size)));
            n = (int) ((((int)x + offset) / color_x_size) + ((int)y / color_y_size) * rowCapacity);
        } else {
            int columnCapacity = (int) ((palette.size() / (mWidth / color_x_size)) + (palette.size() % (mWidth / color_x_size)));
            n = (int) ((((int)x / color_x_size) * columnCapacity) + (((int)y + offset) / color_y_size));
        }
        if(n >= 0 && n < palette.size()) {
            if(callback != null) callback.colorChosen(n);
            chosenColor = n;
            invalidate();
        }
    }

    private void move(float dx, float dy) {
        if(mWidth > mHeight) {
            int rowCapacity = (int) ((palette.size() / (mHeight / color_y_size)) + (palette.size() % (mHeight / color_y_size)));
            if((offset - dx ) < 0) offset = 0;
            else if((offset - dx) > (rowCapacity * color_x_size - mWidth)) offset = rowCapacity * color_x_size - mWidth;
            else offset -= dx;
        } else {
            int columnCapacity = (int) ((palette.size() / (mWidth / color_x_size)) + (palette.size() % (mWidth / color_x_size)));
            if((offset - dy ) < 0) offset = 0;
            else if((offset - dy) > (columnCapacity * color_y_size - mHeight)) offset = columnCapacity * color_y_size - mHeight;
            else offset -= dy;
        }
        invalidate();
    }

    private void measureSize() {
        if(mWidth > 0 && palette != null) {
            int n = 1;
            if(mWidth > mHeight){
                if(mHeight >= 100) n = (int) (mHeight / 75);
                color_x_size = (int) (mWidth / ((palette.size() / n) + (palette.size() % n)));
                if(color_x_size < 100) color_x_size = 100;
                color_y_size = (int) (mHeight / n);
            } else {
                if(mWidth >= 200) n = (int) (mWidth / 100);
                color_x_size = (int) (mWidth / n);
                color_y_size = (int) (mHeight / ((palette.size() / n) + (palette.size() % n)));
                if(color_y_size < 100) color_y_size = 100;
            }
        }
    }

    public interface ClickCallback {
        void colorChosen(int n);
    }
}
