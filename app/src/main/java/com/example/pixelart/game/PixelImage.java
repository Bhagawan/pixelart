package com.example.pixelart.game;

import android.graphics.Bitmap;
import android.graphics.Color;

import java.util.ArrayList;

public class PixelImage {
    private ImageCell[][] field;
    private ArrayList<Integer> palette = new ArrayList<>();
    private int mWidth, mHeight, mSize;

    public PixelImage(Bitmap bitmap) {
        mSize = getSize(bitmap);
        mWidth = bitmap.getWidth() / mSize;
        mHeight = bitmap.getHeight() / mSize;
        field = new ImageCell[mWidth][mHeight];

        int color;
        for(int x = 0; x < (bitmap.getWidth() / mSize); x++) {
            for(int y = 0; y < (bitmap.getHeight() / mSize); y++) {
                color = bitmap.getPixel(x * mSize,y * mSize);
                if(color == 0 ) {
                    field[x][y] = null;
                } else {
                    if(!palette.contains(color)) palette.add(color);
                    int i = palette.indexOf(color);
                    field[x][y] = new ImageCell(palette.indexOf(color));
                }
            }
        }
    }

    public ImageCell getPixel(int x, int y) {
        return field[x][y];
    }

    public void PaintPixel(int x, int y, int color) {
        field[x][y].setCurrentColor(color);
    }

    public ArrayList<Integer> getPalette() {
        return palette;
    }

    public int getColor(int num) {
        if(num >= 0 && num < palette.size()) return palette.get(num);
        else return Color.WHITE;
    }

    public int getWidth() {
        return mWidth;
    }

    public int getHeight() {
        return mHeight;
    }

    private int getSize(Bitmap bitmap) {
        int color;
        int pixel = 0;
        int size = 0;
        int minSize = 0;

        for(int i = 0; i < bitmap.getWidth(); i++) {
            for(int j = 0; j < bitmap.getHeight(); j++) {
                color = bitmap.getPixel(i,j);
                if(color != 0 && Color.alpha(color) != 0 && color != -1 ) {
                    if(pixel == 0) {
                        pixel = color;
                        size++;
                    } else if (pixel == color) {
                        size++;
                    } else {
                        if(minSize == 0 && size != 0) minSize = size;
                        else if (minSize > size && size != 0) minSize = size;
                        pixel = color;
                        size = 1;
                    }
                } else {
                    if(minSize == 0 && size != 0) minSize = size;
                    else if (minSize > size && size != 0) minSize = size;
                    size = 0;
                }
            }
            if(minSize == 0 && size != 0) minSize = size;
            else if (minSize > size && size != 0) minSize = size;
            pixel = 0;
            size = 0;
        }
        return minSize;
    }

    private int getSize2(Bitmap bitmap) {
        int color;
        int firstPixel = 0;
        int size = 0;

        for(int i = 0; i < bitmap.getWidth(); i++) {
            for(int j = 0; j < bitmap.getHeight(); j++) {
                color = bitmap.getPixel(i,j);
                if(color != 0 || Color.alpha(color) != 0) {
                    if(firstPixel == 0){
                        firstPixel = color;
                        size++;
                    }
                    else {
                        if(color == firstPixel){
                            size++;
                        }
                        else return size;
                    }
                } else if(firstPixel != 0 && size > 0) return size;
            }
            firstPixel = 0;
            size = 0;
        }

        return size;
    }

}
