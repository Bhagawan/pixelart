package com.example.pixelart.game;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.Nullable;

import java.util.ArrayList;

public class Game extends View {
    private int pixel_size = 50;
    private int text_size = (int)(pixel_size / 1.5);

    private float mHeight;
    private float mWidth;
    private PixelImage pixelImage;

    private float touchX,touchY;
    private float offX, offY;
    private boolean click = false;
    private float secondX, secondY;
    private int secondID = -1;
    private double distance = 0;
    private int currColor = 0;
    private int paletteSize = 0;

    private GameCallback callback;

    public Game(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }


    @Override
    protected void onDraw(Canvas c) {
        if(mWidth > 0 && pixelImage != null){
            drawField(c);
        }
    }

    @Override
    protected void onSizeChanged(int xNew, int yNew, int xOld, int yOld) {
        super.onSizeChanged(xNew, yNew, xOld, yOld);
        if(getWidth() > 0) initialise();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if(event.getActionMasked() == MotionEvent.ACTION_DOWN) {
            if(event.getPointerId(event.getActionIndex()) == 0) {
                touchX = event.getX();
                touchY = event.getY();
                click = true;
            }
            return true;
        } else if(event.getActionMasked() == MotionEvent.ACTION_MOVE) {
            if(secondID < 0) {
                moveTo(event.getX() - touchX, event.getY() - touchY);
                touchX = event.getX();
                touchY = event.getY();
            } else {
                if(event.getPointerId(event.getActionIndex()) == secondID) {
                    secondX = event.getX(secondID);
                    secondY = event.getY(secondID);
                } else {
                    touchX = event.getX();
                    touchY = event.getY();
                }
                resize();
            }
            click = false;
            return true;
        } else if(event.getActionMasked() == MotionEvent.ACTION_UP) {
            if(click && secondID < 0 ) paint(event.getX(), event.getY());
            click = false;
            return true;
        } else if(event.getActionMasked() == MotionEvent.ACTION_POINTER_DOWN) {
            secondID = event.getPointerId(event.getActionIndex());
            secondX = event.getX(secondID);
            secondY = event.getY(secondID);
            return true;
        } else if(event.getActionMasked() == MotionEvent.ACTION_POINTER_UP) {
            secondID = -1;
            distance = 0;
            return true;
        }
        return super.onTouchEvent(event);
    }

    public void setPixelImage(PixelImage pixelImage) {
        this.pixelImage = pixelImage;
        offX = (float)pixelImage.getWidth() * pixel_size / 2;
        offY = (float)pixelImage.getHeight() * pixel_size / 2;
        paletteSize = pixelImage.getPalette().size();
        invalidate();
    }

    public void setCurrentColor(int color) {
        this.currColor = color;
    }

    public void setCallback(GameCallback callback) {
        this.callback = callback;
    }

    public boolean checkPicture() {
        ImageCell cell;
        boolean complete = true;
        for(int x = 0; x < pixelImage.getWidth(); x++) {
            for(int y = 0; y < pixelImage.getHeight(); y++) {
                cell = pixelImage.getPixel(x, y);
                if(cell != null) {
                    if(cell.getColor() != cell.getCurrentColor()) {
                        complete = false;
                        cell.setCurrentColor(-1);
                    }
                }
            }
        }
        invalidate();
        return complete;
    }

    private void initialise() {
        mWidth = getWidth();
        mHeight = getHeight();
        if(offX < mWidth / 2) offX = -((mWidth / 2) - (float)(pixelImage.getWidth() * pixel_size / 2));
        if(offY < mHeight / 2) offY = -((mHeight / 2) - (float)(pixelImage.getHeight() * pixel_size / 2));
        invalidate();
    }

    private void drawField(Canvas c) {
        Paint p = new Paint();
        Paint t = new Paint();
        t.setTextAlign(Paint.Align.CENTER);
        t.setTextSize(text_size);
        t.setColor(Color.BLACK);
        ImageCell pixel;
        for(int x = 0; x < pixelImage.getWidth(); x++) {
            for(int y = 0; y < pixelImage.getHeight(); y++) {
                pixel = pixelImage.getPixel(x,y);
                if(pixel != null) {
                    if(pixel.getCurrentColor() != -1) {
                        p.setStyle(Paint.Style.FILL);
                        p.setColor(pixelImage.getColor(pixel.getCurrentColor()));
                    } else {
                        p.setStyle(Paint.Style.STROKE);
                        p.setStrokeWidth(3);
                        p.setColor(Color.BLACK);
                        c.drawText(String.valueOf(pixel.getColor() + 1),(x * pixel_size) + ((float)pixel_size / 2) - offX, y * pixel_size + (int)((pixel_size - text_size) / 2) + text_size - offY, t);
                    }
                }
                else {
                    p.setStyle(Paint.Style.FILL);
                    p.setColor(Color.WHITE);
                }
                c.drawRect(x * pixel_size - offX, y * pixel_size - offY,
                        (x + 1) * pixel_size - offX, (y + 1) * pixel_size - offY, p);
            }
        }
    }

    private void moveTo(float x, float y) {
        float w = pixelImage.getWidth() * pixel_size;
        float h = pixelImage.getHeight() * pixel_size;
        if(w < mWidth) {
            offX = -((mWidth / 2) - (float)(pixelImage.getWidth() * pixel_size / 2));
        } else if(offX - x < - (mWidth / 2)) {
            offX = - (mWidth / 2);
        } else if(offX - x > w - (mWidth / 2)) {
            offX = w - mWidth / 2;
        } else {
            offX -= x;
        }
        if(h < mHeight) {
            offY = -((mHeight / 2) - (float)(pixelImage.getHeight() * pixel_size / 2));
        } else if(offY - y < - (mHeight / 2)) {
            offY = - (mHeight / 2);
        } else if(offY - y > h - (mHeight / 2)) {
            offY = h - mHeight / 2;
        } else {
            offY -= y;
        }
        invalidate();
    }

    private void paint(float x, float y) {
        int pX = (int) ((x + offX)/ pixel_size);
        int pY = (int) ((y + offY) / pixel_size);
        if(pX >= 0 && pX < pixelImage.getWidth() && pY >= 0 && pY < pixelImage.getHeight()){
            ImageCell pixel = pixelImage.getPixel(pX, pY);
            if(pixel != null) pixel.setCurrentColor(currColor);
            updateCompletion();
            invalidate();
        }
    }

    private void resize() {
        double l = Math.sqrt((Math.pow(secondX - touchX, 2) + Math.pow(secondY - touchY, 2)));
        if(distance == 0) distance = l;
        int a = (int)((l - distance) / (mWidth / 20));
        float centerX = (secondX + touchX) / 2;
        float centerY = (secondY + touchY) / 2;
        float fieldX = (centerX + offX) / pixel_size;
        float fieldY =(centerY + offY) / pixel_size;


        if(pixel_size + a <= 20) {
            moveTo(-((20 - pixel_size) * fieldX), -((20 -pixel_size) * fieldY));
            pixel_size = 20;
        }
        else if(pixel_size + a >= 100) {
            moveTo(-((100 -pixel_size) * fieldX), -((100 -pixel_size) * fieldY));
            pixel_size = 100;
        }
        else {
            pixel_size += a;
            moveTo(-(a * fieldX), -(a * fieldY));
        }
        text_size =  (int)(pixel_size / 1.5);
        invalidate();
    }

    private void updateCompletion() {
        ImageCell cell;
        ArrayList<Boolean> complete = new ArrayList<>();
        for(int n = 0; n < paletteSize; n++) {
            complete.add(true);
        }
        for(int x = 0; x < pixelImage.getWidth(); x++) {
            for(int y = 0; y < pixelImage.getHeight(); y++) {
                cell = pixelImage.getPixel(x, y);
                if(cell != null) {
                    if(cell.getColor() != cell.getCurrentColor()) {
                        complete.set(cell.getColor(),false);
                    }
                }
            }
        }
        if(callback != null) callback.completionChange(complete);
    }

    public interface GameCallback {
        void completionChange(ArrayList<Boolean> complete);
    }

}
