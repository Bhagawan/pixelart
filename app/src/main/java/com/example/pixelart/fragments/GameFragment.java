package com.example.pixelart.fragments;

import android.graphics.Bitmap;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.example.pixelart.R;
import com.example.pixelart.game.Game;
import com.example.pixelart.game.PaletteView;
import com.example.pixelart.game.PixelImage;

import java.util.Timer;
import java.util.TimerTask;


public class GameFragment extends Fragment {
    private View mView;
    private Game game;
    private PixelImage pixelImage;

    public GameFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_game, container, false);

        if(getArguments() != null){
            Bitmap b = getArguments().getParcelable("image");
            pixelImage = new PixelImage(b);
        }

        if(pixelImage != null) {
            game = mView.findViewById(R.id.game);
            game.setPixelImage(pixelImage);

            PaletteView paletteView = mView.findViewById(R.id.paletteView);
            paletteView.setPalette(pixelImage.getPalette());
            paletteView.setCallback(game::setCurrentColor);

            game.setCallback(paletteView::updateCompletion);
        }

        ImageButton backButton = mView.findViewById(R.id.btn_game_back);
        backButton.setOnClickListener(v -> backAction());

        ImageButton checkButton = mView.findViewById(R.id.btn_game_check);
        checkButton.setOnClickListener(v -> checkAction());

        return mView;
    }

    private void backAction() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

        builder.setMessage(getContext().getString(R.string.msg_back));
        builder.setPositiveButton(getContext().getString(R.string.action_positive), (dialog, which) -> getParentFragmentManager().popBackStack());

        builder.setNegativeButton(getContext().getString(R.string.action_negative), (dialog, which) -> dialog.dismiss());

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void checkAction() {
        if(pixelImage != null)
        if(game.checkPicture()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

            builder.setMessage(getContext().getString(R.string.msg_congratulation));

            AlertDialog alertDialog = builder.create();
            alertDialog.show();

            Timer time  = new Timer();
            time.schedule(new TimerTask() {
                @Override
                public void run() {
                    alertDialog.dismiss();
                    getParentFragmentManager().popBackStack();
                }
            }, 2000);
        }
    }
}