package com.example.pixelart.fragments;

import android.graphics.Bitmap;
import android.os.Bundle;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;

import com.example.pixelart.R;
import com.example.pixelart.mvp.ChoicePresenter;
import com.example.pixelart.mvp.ChoicePresenterViewInterface;
import com.example.pixelart.util.ChoiceAdapter;
import com.example.pixelart.util.GridDecoration;

import java.util.ArrayList;

import moxy.MvpAppCompatFragment;
import moxy.presenter.InjectPresenter;


public class ChoiceFragment extends MvpAppCompatFragment implements ChoicePresenterViewInterface {
    private View mView;

    @InjectPresenter
    ChoicePresenter mPresenter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_choice, container, false);

        RecyclerView recyclerView = mView.findViewById(R.id.recycler_choice);

        recyclerView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if(recyclerView.getWidth() > 0) {
                    mPresenter.loadPictures();
                    recyclerView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }
            }
        });
        return mView;
    }

    @Override
    public void loadPictures(ArrayList<Bitmap> pictures) {
        RecyclerView recyclerView = mView.findViewById(R.id.recycler_choice);
        final float scale = recyclerView.getResources().getDisplayMetrics().density;
        int px = (int) (150 * scale + 0.5f);
        int padd = (recyclerView.getWidth() % px) / (2 * recyclerView.getWidth() / px);
        recyclerView.addItemDecoration(new GridDecoration(padd));
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), recyclerView.getWidth() / px));
        ChoiceAdapter adapter = new ChoiceAdapter(pictures);
        adapter.setListener(n -> {
            GameFragment gameFragment = new GameFragment();
            Bundle bundle = new Bundle();
            bundle.putParcelable("image", pictures.get(n));
            gameFragment.setArguments(bundle);
            getParentFragmentManager().beginTransaction().replace(R.id.fragmentContainerView, gameFragment)
                    .addToBackStack(null).commit();
        });
        recyclerView.setAdapter(adapter);
    }

}