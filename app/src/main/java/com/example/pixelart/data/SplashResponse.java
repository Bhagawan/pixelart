package com.example.pixelart.data;

import com.google.gson.annotations.SerializedName;

public class SplashResponse {
    @SerializedName("url")
    public String url;
}
