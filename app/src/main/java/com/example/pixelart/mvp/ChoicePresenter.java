package com.example.pixelart.mvp;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;

import androidx.annotation.NonNull;

import com.example.pixelart.util.MyServerClient;
import com.example.pixelart.util.ServerClient;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import moxy.InjectViewState;
import moxy.MvpPresenter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@InjectViewState
public class ChoicePresenter extends MvpPresenter<ChoicePresenterViewInterface> {
    private static final String URL = "http://159.69.89.54/PixelArt/pictures/";

    public void loadPictures() {
        ServerClient client = MyServerClient.createService(ServerClient.class);
        Call<List<String>> call = client.getPictures();
        call.enqueue(new Callback<List<String>>() {
            @Override
            public void onResponse(@NonNull Call<List<String>> call, @NonNull Response<List<String>> response) {
                if(response.body() != null) load(response.body());
            }

            @Override
            public void onFailure(@NonNull Call<List<String>> call, @NonNull Throwable t) {

            }
        });
    }

    private void load(List<String> names) {
        ArrayList<Bitmap> bitmaps = new ArrayList<>();
        ExecutorService executor = Executors.newSingleThreadExecutor();
        Handler handler = new Handler(Looper.getMainLooper());

        for(int i = 0; i < names.size(); i++) {
           String file = URL + names.get(i);
           executor.execute(() -> {
               Bitmap b;
               try {
                   b = Picasso.get().load(file).get();
                   bitmaps.add(b);
                   if (bitmaps.size() == names.size()) {
                       handler.post(() -> getViewState().loadPictures(bitmaps));
                   }
               } catch (IOException e) {
                   e.printStackTrace();
               }
           });
        }
    }
}
