package com.example.pixelart.mvp;

import android.graphics.Bitmap;

import java.util.ArrayList;

import moxy.MvpView;
import moxy.viewstate.strategy.alias.OneExecution;

public interface ChoicePresenterViewInterface extends MvpView {

    @OneExecution
    void loadPictures(ArrayList<Bitmap> pictures);
}
