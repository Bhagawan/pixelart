package com.example.pixelart;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.pixelart.data.SplashResponse;
import com.example.pixelart.util.MyServerClient;
import com.example.pixelart.util.MyWebChromeClient;
import com.example.pixelart.util.ServerClient;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SplashActivity extends AppCompatActivity {
    private WebView mWebView;

    @Override
    public void onBackPressed() {
        if (mWebView!=null){
            if (mWebView.canGoBack()){
                mWebView.goBack();
                return;
            }
        } else finish();
        super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        mWebView = findViewById(R.id.webView_splash);
//        mWebView = new WebView(this);
//        setContentView(mWebView);
        initialiseWebView();
        getWindow().getDecorView().setOnSystemUiVisibilityChangeListener(visibility -> fullscreen());
        fullscreen();
        if(savedInstanceState == null) runAdd();
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState )
    {
        super.onSaveInstanceState(outState);
        mWebView.saveState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState)
    {
        super.onRestoreInstanceState(savedInstanceState);
        mWebView.restoreState(savedInstanceState);
    }

    public void fullscreen() {
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);

    }

    private void runAdd() {
        ServerClient client = MyServerClient.createService(ServerClient.class);
        Call<SplashResponse> call = client.getSplash(Locale.getDefault().getLanguage());
        call.enqueue(new Callback<SplashResponse>() {
            @Override
            public void onResponse(@NonNull Call<SplashResponse> call, @NonNull Response<SplashResponse> response) {
                if (response.isSuccessful()) {
                    String s = response.body().url;
                    if(s.equals("no")) {
                        switchToMain();
                    }
                    else {
                        mWebView.loadUrl("https://" + s);
                    }
                } else {
                    Toast.makeText(getApplicationContext(),"Server error 2", Toast.LENGTH_SHORT).show();
                    switchToMain();
                }
            }

            @Override
            public void onFailure(@NonNull Call<SplashResponse> call, @NonNull Throwable t) {
                Toast.makeText(getApplicationContext(),"Server error 3", Toast.LENGTH_SHORT).show();
                switchToMain();
            }
        });
    }

    private void switchToMain() {
        Intent i = new Intent(getApplicationContext(), MainActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK |Intent.FLAG_ACTIVITY_NO_ANIMATION);
        getApplicationContext().startActivity(i);
        finish();
    }

    private  void initialiseWebView() {
        FrameLayout frame = findViewById(R.id.webView_customView);

        mWebView.setWebViewClient(new WebViewClient());
        mWebView.setWebChromeClient(new MyWebChromeClient(this, mWebView, frame));
        WebSettings webSettings = mWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setAllowContentAccess(true);
        webSettings.setSaveFormData(true);
        webSettings.setAppCacheEnabled(true);
        webSettings.setAllowFileAccess(true);

        String userAgent = System.getProperty("http.agent");
        webSettings.setUserAgentString(userAgent + getString(R.string.app_name));
    }
}