package com.example.pixelart.util;


import com.example.pixelart.data.SplashResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ServerClient {

    @GET("PixelArt/pictures.json")
    Call<List<String>> getPictures();

    @FormUrlEncoded
    @POST("PixelArt/splash.php")
    Call<SplashResponse> getSplash(@Field("locale") String locale);

}
