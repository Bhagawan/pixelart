package com.example.pixelart.util;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.view.View;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.FrameLayout;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

public class MyWebChromeClient extends WebChromeClient {
    private ValueCallback<Uri[]> mCallback;
    private AppCompatActivity activity;
    private View mCustomView;
    private int mOriginalSystemUiVisibility;
    private int mOriginalOrientation;
    private CustomViewCallback mCustomViewCallback;
    private WebView webView;
    private FrameLayout frameLayout;

    private ActivityResultLauncher<Intent> mLauncher;

    public MyWebChromeClient(AppCompatActivity activity, WebView webView, FrameLayout frameLayout) {
        this.activity = activity;
        this.webView = webView;
        this.frameLayout = frameLayout;
        mLauncher = activity.registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), result -> {
            if(result != null) {
                String dataString = result.getData().getDataString();
                if (dataString != null && mCallback != null) {
                    Uri[] uri = new Uri[]{Uri.parse(dataString)};
                    mCallback.onReceiveValue(uri);
                }
            }
        });
    }

    @Override
    public Bitmap getDefaultVideoPoster() {
        return Bitmap.createBitmap(10, 10, Bitmap.Config.ARGB_8888);
    }

    @Override
    public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> filePathCallback, FileChooserParams fileChooserParams) {
        if (mCallback != null) {
            filePathCallback.onReceiveValue(null);
        }
        mCallback = filePathCallback;

        if(mLauncher != null) mLauncher.launch(fileChooserParams.createIntent());
        return true;
    }

    @Override
    public void onHideCustomView() {
        mCustomView = null;
        webView.setVisibility(View.VISIBLE);
        frameLayout.setVisibility(View.GONE);
        mCustomViewCallback.onCustomViewHidden();
        mCustomViewCallback = null;
        activity.getWindow().getDecorView().setSystemUiVisibility(mOriginalSystemUiVisibility);
        activity.setRequestedOrientation(mOriginalOrientation);
    }

    @Override
    public void onShowCustomView(View view, int requestedOrientation, CustomViewCallback callback) {
        onShowCustomView(view, callback);
    }

    @Override
    public void onShowCustomView(View view, CustomViewCallback callback) {
        if (mCustomView != null) {
            onHideCustomView();
            return;
        }
        webView.setVisibility(View.GONE);
        mCustomView = view;
        mOriginalSystemUiVisibility = activity.getWindow().getDecorView().getSystemUiVisibility();
        mOriginalOrientation = activity.getRequestedOrientation();
        mCustomViewCallback = callback;
        frameLayout.addView(mCustomView,
                new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT,
                        FrameLayout.LayoutParams.MATCH_PARENT));
        frameLayout.setVisibility(View.VISIBLE);
        activity.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
    }
}